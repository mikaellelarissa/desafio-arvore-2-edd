package domain;

import java.util.Scanner;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		Arvore raiz = null;
		Integer esc, numero;

		do {
			System.out.println("(1) Preencher a �rvore \n(2) Consultar em ordem \n(3) Consultar pr�-ordem \n(4) Consultar p�s-ordem \n(5) Sair ");
			esc = scan.nextInt();
			scan.nextLine();
			switch(esc) {
			case 1: {
				raiz = null;
				for(int i = 1; i <= 10; i++) {
					System.out.println("Informe o n�mero que deseja inserir: ");
					numero = scan.nextInt();
					scan.nextLine();

					raiz = inserir(raiz, numero);
				}
				break;
			}
			case 2: {
				if(raiz == null) {
					System.out.println("Sua �rvore est� vazia!");
				} else {
					emOrdem(raiz);
				}
				break;
			}
			case 3: {
				if(raiz == null) {
					System.out.println("Sua �rvore est� vazia!");
				} else {
					preOrdem(raiz);
				}
				break;
			}
			case 4: {
				if(raiz == null) {
					System.out.println("Sua �rvore est� vazia!");
				} else {
					posOrdem(raiz);
				}
				break;
			}
			default: {
				System.out.println("At�!");
			}
			}
		} while(esc != 5);
	}

	public static Arvore inserir(Arvore aux, Integer num) {
		if(aux == null) {
			aux = new Arvore();
			aux.numero = (num * 100);
			aux.direita = null;
			aux.esquerda = null;
		} else if(num < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, num);
		} else {
			aux.direita = inserir(aux.direita, num);
		}
		return aux;
	}


	  public static void emOrdem(Arvore aux) {
	    if(aux != null) {
	    	emOrdem(aux.esquerda);
	    	System.out.println(aux.numero);
	    	emOrdem(aux.direita);
	    }
	  }
	  
	  public static void preOrdem(Arvore aux) {
	    if(aux != null) {
	    	System.out.println(aux.numero);
	    	preOrdem(aux.esquerda);
	    	preOrdem(aux.direita);
	    }
	  }
	  
	  public static void posOrdem(Arvore aux) {
	    if(aux != null) {
	    	posOrdem(aux.esquerda);
	    	posOrdem(aux.direita);
	    	System.out.println(aux.numero);
	    }
	  }  

}
